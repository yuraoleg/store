$(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      responsive: true,
      navigation: true,
      pagination: false,
      items : 4,
      itemsDesktop : [1940,4],
    itemsDesktopSmall : [1940,3],
    itemsTablet: [768,2],
    itemsTabletSmall: false,
    itemsMobile : [479,1],
    navigationText : ["",""],
 
  });
 
});